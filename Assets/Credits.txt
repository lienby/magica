Plugins used:

-- Yanfly (http://yanfly.moe) --
YEP_CoreEngine
YEP_SaveCore
YEP_TouchInputDisabler
YEP_MessageCore
YEP_EventSpriteOffset
YEP_X_ExtMesPack1
YEP_KeyboardConfig

-- SilicaAndPina --
Magica! Custom plugins~
SIL_CUSTOM_GameData, 
DEBUG_GameData,

-- Yoji Ojima, Sasuke KANNAZUKI / Offcial MV Plugins (https://www.rpgmakerweb.com) --
MV Default Plugins
(Maybe remove? not acturally using any)

-- Galv (https://galvs-scripts.com) --
GALV_DiagonalMovement

-- SumRndmDde (http://sumrndm.site) --
SRD_CameraCore

-- MogHunter / Atelier RRGSS (https://atelierrgss.wordpress.com) --
MOG_MenuCursor

-- Masked (https://github.com/masked-rpgmaker) --
MBS_MapZoom

-- HimeWorks (https://atelierrgss.wordpress.com) --
HIME_SyncSaveData
HIME_LargeChoices

-- Tor Damian Design (https://mvplugins.tordamian.com) --
TDDP_BindPicturesToMap
TDDP_PreloadManager

-- Hudell (https://github.com/Hudell/mv-plugins) --
OrangeMapShot

Art:
SilicaAndPina

Characters created with KHMIX
http://khmix.sakura.ne.jp

Enterbrain / DEGCIA / KADOKAWA
RPG Maker RTP

Music:
Aaron Krogh

SE:
Buzzer:
distillerystudio
https://freesound.org/people/distillerystudio/sounds/327736/
Cursor:
joedeshon
https://freesound.org/people/joedeshon/sounds/119415/
Click: 
Heshl
https://freesound.org/people/Heshl/sounds/269136/
Cancel: 
Raclure
https://freesound.org/people/Raclure/sounds/405548/
Load Save:
Raclure
https://freesound.org/people/Raclure/sounds/405546/
Delete Save: 
MortisBlack
https://freesound.org/people/MortisBlack/sounds/385046/
Save Save:
Blackie666
https://freesound.org/people/Blackie666/sounds/111694/
GAME ERROR SOUND:
fisch12345
https://freesound.org/people/fisch12345/sounds/325113/
Teleport Sound:
Andromadax24
https://freesound.org/people/Andromadax24/sounds/178347/
Equip Sound:
Loyalty_Freak_Music
https://freesound.org/people/Loyalty_Freak_Music/sounds/407481/

Jump/Alarmed Sound:
ENTERBRAIN / RPG MAKER XP RTP

PC Turn on/off & Running sound:
Edited version of:
Zabuhailo
https://freesound.org/people/Zabuhailo/sounds/146957/ 