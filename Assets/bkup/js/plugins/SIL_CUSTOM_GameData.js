/*:
 * @plugindesc Various mods to the Game Engine Code *.js (This plugin is specific to Magica!)
 * @author SilicaAndPina
 */
 
if(document.title != "Magica!")
{
	window.alert("SIL_CUSTOM_GameData.js loaded, but not on Magica?\n\nI see 3 possiblitys\n1) You Stole it\n2) You Stole it\n3) YOU STOLE IT!!");
	PIXI = [];
}

var DEBUG_MODE = false;
var lastMap = 0;
const util = require('util');
const exec = util.promisify(require('child_process').exec)

/*
*	Small changes to various Plugin/Vanilia functions..
*/ 

//Disable Map Name Windows.
Window_MapName.prototype.open = function(){};

//Disable non-volume options from options Menu
Window_Options.prototype.addGeneralOptions = function(){}

//Fix TDDP_PreloadManager "File does not exist" error
PreloadManager.controlFileExistence = function(folder, filename, extensions) {}

/*
*	Function definitions
*/

MenuIsOpen = false; // Used to make reloading saves work properly.

async function runCommand(command) {
  const { stdout, stderr } = await exec(command);
  if(DEBUG_MODE)
  {
	console.log('stdout:', stdout);
	console.log('stderr:', stderr);
  }
}

/*
*Based off PrimeHover PH_SwapPartyPlugin.js
*/

function movePartyRight()
{
	if(MenuIsOpen)
	{
		return false;
	}
	if($gameParty._actors._length != 1)
	{
		i = 0
		temp = $gameParty._actors[0];
		while(i < $gameParty._actors.length)
		{
			if((i + 1) === $gameParty._actors.length)
			{
				$gameParty._actors[i] = temp
			}
			else
			{
				$gameParty._actors[i] = $gameParty._actors[i + 1];
			}
			i += 1
		}
		$gamePlayer.refresh();
	}	
}

function movePartyLeft()
{
	if(MenuIsOpen)
	{
		return false;
	}
	if($gameParty._actors._length != 1)
	{
		i = $gameParty._actors.length -1
		temp = $gameParty._actors[$gameParty._actors.length - 1];
		while(i >= 0)
		{
			if(i === 0)
			{
				$gameParty._actors[i] = temp
			}
			else
			{
				$gameParty._actors[i] = $gameParty._actors[i - 1];
			}
			i -= 1
		}
		$gamePlayer.refresh();
	}
}	

/*
*Based off PrimeHover PH_SwapPartyPlugin.js
* --END--
*/

function changeScaleX(pictureId,newX)
{
	newX = newX / $gameMap.zoom.x;
	$gameScreen.picture(pictureId)._scaleX = newX
	$gameScreen.picture(pictureId)._targetScaleX = newX
	var name = $gameScreen.picture(pictureId)._name
	if(DEBUG_MODE)
	{
		console.log("name "+name+"("+pictureId+")")
		console.log("newx "+newX)
	}
}

function saveGameSave(slot)
{
	return DataManager.saveGameWithoutRescue(slot)
}

function transferPosition(MapId,X,Y)
{
	if(DEBUG_MODE)
	{
		console.log("Moving to Map #"+MapId+" X:"+X+" Y:"+Y);
	}
	$gamePlayer._fadeType = 2;
	SceneManager.goto(Scene_Map);
	$gamePlayer._newX = X; 
	$gamePlayer._newY = Y;
	$gamePlayer._newMapId = MapId;
	$gamePlayer._transferring = true;

}


function transferEvent(eventid,x,y)
{
	var spd = $gameMap.event(eventid)._moveSpeed;
	$gameMap.event(eventid)._moveSpeed = 999;
	$gameMap.event(eventid)._x = x;
	$gameMap.event(eventid)._y = y;
	$gameMap.updateEvents();
	$gameMap.event(eventid)._moveSpeed = spd;
}


//Hook: DataManager.isDatabaseLoaded
DataManager.isDatabaseLoadedOriginal = DataManager.isDatabaseLoaded;
DataManager.isDatabaseLoaded = function()
{
	var ret = DataManager.isDatabaseLoadedOriginal();
	if(ret && !Utils.isOptionValid('test') && $dataCommonEvents[1].name != "AllwaysRun" && Math.randomInt(100) <= 15)
	{
		window.alert("SIL_CUSTOM_GameData.js stolen from Magica!\nWhen i told them cannot use this plugin\nthey tried to patch out my check (and failed)");
		PIXI = [];
	}
	return ret;
}

//Hook: Game_Screen.prototype.startTint()
Game_Screen.prototype.startTintOriginal = Game_Screen.prototype.startTint;
Game_Screen.prototype.startTint = function(tone, duration)
{
	this.startTintOriginal(tone, duration);
	
	len = this._pictures.length;
	for(i = 0; i<len; i++)
	{
		$gameScreen.tintPicture(i,tone,duration);
	}
}	

//Hook: SceneManager.goto()
SceneManager.gotoOrginal = SceneManager.goto; 
SceneManager.goto = function(sceneClass){
	if(DEBUG_MODE)
	{
		console.log("Attempting to goto scene: "+sceneClass.name);
	}
	//Setup MAP-BASED TitleScreen (and gameover lol)!!
	//Whats HIME_PreTitleEvents.js?! ive.. never heard of it ^-^

	if(sceneClass.name === "Scene_Title") //If the game is trying to goto Scene_Title
	{
		
		if(DEBUG_MODE)
		{
			console.log("Scene goto interrupted! (SCENE_TITLE_HOOK)");
		}
		if(SceneManager._scene.constructor.name != "Scene_Boot") //And. if the game is already running... (not on Scene_Boot)
		{
			SceneManager.push(Scene_Boot); //"restart" the game (this means that variables and stuff are still reset after returning to title screen!)
		}
		else
		{
			transferPosition(1,0,0); //Transfer player to our Title-Map!!
		}
	}
	else if(sceneClass.name === "Scene_Gameover") //If the game is trying to goto Scene_Title
	{
		if(DEBUG_MODE)
		{
			console.log("Scene goto interrupted! (SCENE_GAMEOVER_HOOK)");
		}
		$gamePlayer._transparent = true; //Make player invisible
		transferPosition(2,0,0); //Transfer player to our GameOver-Map!!
	}
	//Run a common-event instead of opening the menu!!
	else if(sceneClass.name === "Scene_Debug")
	{
		if(DEBUG_MODE)
		{
			console.log("Scene goto interrupted! (SCENE_DEBUG_HOOK)");
			$gameTemp.reserveCommonEvent(2);
		}
	}
	//Run a common-event instead of opening the menu!!
	else if(sceneClass.name === "Scene_Menu")
	{
		if(DEBUG_MODE)
		{
			console.log("Scene goto interrupted! (SCENE_MENU_HOOK)");
		}
		$gameTemp.reserveCommonEvent(14);
	}
	else
	{
		return SceneManager.gotoOrginal(sceneClass)
	}
}

//Hook Game_Temp.prototype.reserveCommonEvent
Game_Temp.prototype.reserveCommonEventOriginal = Game_Temp.prototype.reserveCommonEvent;
Game_Temp.prototype.reserveCommonEvent = function(id)
{
	if(DEBUG_MODE)
	{
		console.log("Reserve Common Event "+id);
	}
	var ret = this.reserveCommonEventOriginal(id);
	if(id >= 30)
		MenuIsOpen = false;
	return ret;
}

//Hook DataManager.loadGame
DataManager.loadGameOriginal = DataManager.loadGame;
DataManager.loadGame = function(slot)
{
	if(DEBUG_MODE)
	{
		console.log("Loading Game #"+slot);
	}
	MenuIsOpen = false;
	return DataManager.loadGameOriginal(slot);
}

/*
*  Dont load selected parallax, (we use notetags instead)
*/

//Hook Game_Map.prototype.setupParallax
Game_Map.prototype.setupParallax = function() 
{
	this._parallaxName = '';
    this._parallaxZero = true;
    this._parallaxLoopX = 0;
    this._parallaxLoopY = 0;
    this._parallaxSx = 0;
    this._parallaxSy = 0;
    this._parallaxX = 0;
    this._parallaxY = 0;
}

//Hook Scene_Map.prototype.onMapLoaded
Scene_Map.prototype.onMapLoadedOrignal = Scene_Map.prototype.onMapLoaded;
Scene_Map.prototype.onMapLoaded = function()
{
	var ret = this.onMapLoadedOrignal();
	var mapChanged = $gameMap.mapId() != lastMap;
	lastMap = $gameMap.mapId();
	
	if($gameMap.mapId() > 0)
	{
		
		
		//Change Vehical BGM's to be indentical to MAP BGM's
		$dataSystem.airship.bgm = $dataMap.bgm;
		$dataSystem.ship.bgm = $dataMap.bgm;
		$dataSystem.boat.bgm = $dataMap.bgm;
		
		if(DEBUG_MODE)
		{
			console.log("Map Loaded airship, ship, and boat BGM's where changed");
		}	
		
		if(mapChanged)
		{
			
			if(DEBUG_MODE)
			{
				console.log("ITS A NEW MAP!!");
			}	
			
			if($dataMap.meta.parallax)
			{
				var ppic = $dataMap.meta.parallax;
				ImageManager.loadParallax(ppic);
				$gameMap.changeParallax(ppic, $dataMap.parallaxLoopX, $dataMap.parallaxLoopY,$dataMap.parallaxSx,$dataMap.parallaxSy);
				if(DEBUG_MODE)
				{
					console.log("Changing parallax to: "+ppic);
				}
			
			}
			
			$gameScreen.erasePicture(11);
			$gameScreen.erasePicture(10);
			$gameScreen.erasePicture(20);
			if($dataMap.meta.below_characters)
			{
				var bpic = $dataMap.meta.below_characters;
				
				if(DEBUG_MODE)
				{
					console.log("Binding: "+bpic+" To map!");
				}
				
				ImageManager.loadPicture(bpic);
				$gameScreen.showPicture(10, bpic, 0, 0, 0, 100, 100, 255, 0);
				Game_Interpreter.prototype.bindPictureToMap(10,true,"below_characters");
			}
			
			if($dataMap.meta.above_characters)
			{
				var apic = $dataMap.meta.above_characters;
						
				if(DEBUG_MODE)
				{
					console.log("Binding: "+apic+" To map!");
				}
				
				ImageManager.loadPicture(apic);
				$gameScreen.showPicture(11, apic, 0, 0, 0, 100, 100, 255, 0);
				Game_Interpreter.prototype.bindPictureToMap(11,true,"above_characters");
			}
			
			isEnabled = true;
			
			if($dataMap.meta.light_switch)
			{
				isEnabled = $gameSwitches.value(Number($dataMap.meta.light_switch))
			}
			
			if(isEnabled)
			{
				if($dataMap.meta.lighting_below_characters)
				{
					var bpic = $dataMap.meta.lighting_below_characters;
					
					if(DEBUG_MODE)
					{
						console.log("Binding: "+bpic+" To map!");
					}
					
					ImageManager.loadPicture(bpic);
					$gameScreen.showPicture(19, bpic, 0, 0, 0, 100, 100, 255, 0);
					Game_Interpreter.prototype.bindPictureToMap(19,true,"below_characters");
				}
				
				if($dataMap.meta.lighting_above_characters)
				{
					var apic = $dataMap.meta.lighting_above_characters;
							
					if(DEBUG_MODE)
					{
						console.log("Binding: "+apic+" To map!");
					}
					
					ImageManager.loadPicture(apic);
					$gameScreen.showPicture(20, apic, 0, 0, 0, 100, 100, 255, 0);
					Game_Interpreter.prototype.bindPictureToMap(20,true,"above_characters");
				}
			}
		
		}
		
	}
	return ret;
	
}


//Hook: Game_Screen.prototype.movePicture
Game_Screen.prototype.movePictureOriginal = Game_Screen.prototype.movePicture
Game_Screen.prototype.movePicture = function(pictureId, origin, x, y, scaleX,scaleY, opacity, blendMode, duration)
{
	try{
		var name = $gameScreen.picture(pictureId)._name
		var oSx = $gameScreen.picture(pictureId)._scaleX
		var oSy = $gameScreen.picture(pictureId)._scaleY
		var oX = $gameScreen.picture(pictureId)._x
		var oY = $gameScreen.picture(pictureId)._y
		if(name.toLowerCase().contains("menu") || name == "pointer")
		{
		
			if(oSx != scaleX)
				scaleX = scaleX / $gameMap.zoom.x;
			if(oSy != scaleY)
				scaleY = scaleY / $gameMap.zoom.y;
			if(oY != y)
				y = y / $gameMap.zoom.y;
			if(oX != x)
				x = x / $gameMap.zoom.x;
			
			return this.movePictureOriginal(pictureId, origin, x, y, scaleX, scaleY, opacity, blendMode, duration)
		
		}
		
	}
	catch(e){
	}
	return this.movePictureOriginal(pictureId, origin, x, y, scaleX,scaleY, opacity, blendMode, duration)
	
}

//Hook: Game_Screen.prototype.showPicture
Game_Screen.prototype.showPictureOriginal = Game_Screen.prototype.showPicture
Game_Screen.prototype.showPicture = function(pictureId, name, origin, x, y, scaleX, scaleY, opacity, blendMode)
{

	//Adjust for zoom.
	if(name.toLowerCase().contains("menu") || name == "pointer")
	{
		scaleX = scaleX / $gameMap.zoom.x;
        scaleY = scaleY / $gameMap.zoom.y;
		y = y / $gameMap.zoom.y;
		x = x / $gameMap.zoom.x;
	}
	
	
	try{
	//Translate & Codes
	if(name.contains("&Party1"))
		name = name.replace("&Party1",$gameParty.members()[0].name())
	if(name.contains("&Party2"))
		name = name.replace("&Party2",$gameParty.members()[1].name())
	if(name.contains("&Party3"))
		name = name.replace("&Party3",$gameParty.members()[2].name())
	if(name.contains("&Party4"))
		name = name.replace("&Party4",$gameParty.members()[3].name())
	return this.showPictureOriginal(pictureId, name, origin, x, y, scaleX, scaleY, opacity, blendMode)
	}
	catch(e){
		return false;
	}
	
}


//Change passable ship tiles:
//Hook: Game_Map.prototype.isShipPassable
Game_Map.prototype.isShipPassable = function(x,y) {
	var bit = 1547;
	if(this.allTiles(x,y)[3] === bit)
	{
		return true;
	}
	else
	{
		return false;
	}
}